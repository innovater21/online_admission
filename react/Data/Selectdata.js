const dataSelect1 = 
[
	{
		title: 'First Year',
		value: 1
	},
	{
		title: 'Second Year',
		value: 2
	},
	{
		title: 'Third Year',
		value: 3
	},
	{
		title: 'Fourth Year',
		value: 4
	}

]

export default dataSelect1;