const dataSelect1 = 
[
	{
		title: 'Open',
		value: 1
	},
	{
		title: 'SC/ST',
		value: 2
	},
	{
		title: 'OBC',
		value: 3
	},
	{
		title: 'TFWS',
		value: 4
	},
	{
		title: 'NT/DT',
		value: 5
	},
	{
		title: 'Others',
		value : 6
	}

]

export default dataSelect1;