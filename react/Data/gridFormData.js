import React from 'react';
// import admission from './Admission.jpg'
// var admImage = require('./Admission.jpg');
const tilesData = [
	{
	img: 'https://lh3.googleusercontent.com/9zTv6DVnX8L1q-wz-iJUK1-bycYAuwCPnFTlyXJpZaF5Y_qyR7XqOXyAACijAEjxaBtMmQ=s158',
    title: 'Admission Form',
    Subheader: 'Submit by 122/17',
    navigate: '/admission',
  },
  {
  	img: 'https://www.somaiya.edu/media/images/ExamForm1.jpg',
    title: 'Examination Form',
    Subheader: 'Submit by 13/3/17',
    navigate: '/reval',
  },
  {
  	img: 'https://lh3.googleusercontent.com/9p7OgRjhUnRx1bNePIyLehQDuxvNFqERJql98aXiaACe4PLN-EAnTV5gpV-KJUUJXPNYsA=s113',
    title: 'Photocopy form',
    Subheader: 'Submit by 13/6/17',
    navigate: '/photocopy',
  },
  {
  	img: 'https://lh3.googleusercontent.com/02HBDS1AbfHxPr37cq9tYY8hghxGIvoKB4OleXNyH2RFhrxHObAA49WN4aRKTpt2TkPu6A=s150',
    title: 'Revaluation Form',
    Subheader: 'Submit by 1/2/17',
    navigate: '/kt'
  }
]

export default tilesData