import { observable, computed } from 'mobx';

export class Secpage_schema {
	@observable secpage_data = {
				
			year_ent : null,
			branch_ent : null,
			fe_ent : null,
			se_ent : null,
			te_ent : null
	}
}

const ps = new Secpage_schema() ;

export default ps;

