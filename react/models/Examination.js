import { observable, computed } from 'mobx';

export class Exam_schema {
	@observable exam_data = {
		exam_sem: null,
		exam_branch: null,
		exam_scheme: null,
		exam_place: '',
		exam_date: '',
		exam_fname: '',
		exam_mname: '',
		exam_lname: '',
		exam_address: '',
		exam_telnum: '',
		exam_caste: null,
		exam_subject1: '',
		exam_subject2: '',
		exam_subject3: '',
		exam_subject4: '',
		exam_subject5: '',
		exam_subject6: ''

	}
}

const ps = new Exam_schema() ;

export default ps;
