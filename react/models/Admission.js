import { observable, computed } from 'mobx';

export class Admission_Schema {
	@observable adm_data = {
		adm_year: '',
		adm_fname: '',
		adm_faname: '',
		adm_lname: '',
		adm_moname: '',
		adm_gender: '', 
		adm_dob: '',
		adm_religon: '',
		adm_blood: '',
		adm_caste: '',
		adm_address1: '',
		adm_address2: '',
		adm_pincode: '',
		adm_phone: '',
		adm_email: '',
		adm_bplace: '',
		adm_splace: '',
		adm_csplace: '',
		adm_aadhar: '',
		adm_branch: '',
		adm_ktfe: null,
		adm_ktse: null,
		adm_ktte: null,
		adm_cgpafe: '',
		adm_cgpase: '',
		adm_cgpate: '',
		adm_attemptfe: '',
		adm_attemptse: '',
		adm_attemptte: '',
		adm_exambodyfe: '',
		adm_exambodyse: '',
		adm_exambodyte: '',
		adm_parentname: '',
		adm_relation: '',
		adm_occ: '',
		adm_parentph: '',
		adm_parentoffad: '',
		adm_residencead: '',
		adm_date: '',
		adm_place: ''
	}
}

const ps = new Admission_Schema() ;

export default ps;
