import React from 'react';
import { observable }from 'mobx';

export class Data {
	
	@observable candidatesData = [];

	getData() {
		return fetch('http://localhost:5000/admissions', {
			method: 'get'
		}).then(r => r.json())
		.then(r => {
			this.candidatesData = r;
			console.log(r)
		})
		.catch(function(err) {console.log(err)});
	}

	getSelectedData(id) {
		return this.candidatesData.filter((obj) => obj.id == id).pop()
	}

	getPhoto(id) {
		return 	fetch('http://localhost:5000/admissions/custshow/' + id, {
			method: 'get'
		})
	}
	
}

const data = new Data();

export default data;
