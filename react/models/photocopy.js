import { observable, computed } from 'mobx';

export class Photocopy_schema {
	@observable photocopy_data = {
		ph_month : null,
		ph_year: null,
		ph_seatnumber: '',
		ph_nameca: '',
		ph_primaryaddca: '',
		ph_secondaryaddca: '',
		ph_districtca: '',
		ph_stateca: '',
		ph_pincodeca: '',
		ph_nameco: '',
		ph_primaryaddco: '',
		ph_secondaryaddco: '',
		ph_districtco: '',
		ph_stateco: '',
		ph_pincodeco: '',
		ph_subjectname: '',
		ph_paperno: '',
		ph_codeno: '',
		ph_date: '',
		ph_time: '',
		ph_marksobtained: ''
	}
}

const ps = new Photocopy_schema() ;

export default ps;

/* 
ph_month : 
		ph_year: 
		ph_seatnumber: 
		ph_nameca: 
		ph_primaryaddca: 
		ph_secondaryaddca: 
		ph_districtca: 
		ph_stateca: 
		ph_pincodeca: 
		ph_nameco: 
		ph_primaryaddco: 
		ph_secondaryaddco: 
		ph_districtco: 
		ph_stateco: 
		ph_pincodeco: 
		ph_subjectname: 
		ph_paperno: 
		ph_codeno: 
		ph_date: 
		ph_time: 
		ph_marksobtained: 
*/