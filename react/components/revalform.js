import React, { Component } from 'react';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Stepone from './BaseComponents/Examination/Partone';
import Steptwo from './BaseComponents/Examination/Parttwo';
import Dialog from './BaseComponents/Examination/DialogExamination';
import Examination from '../models/Examination';
import 'whatwg-fetch';

class Exam extends Component {

state = {
    finished: false,
    stepIndex: 0,
  };

  handleSubmit() {
  fetch("http://192.168.1.6:5000/examinations",{
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({ examinations: Examination.exam_data })
}).then(r => console.log(r))
}
  
  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex < 1) {
    this.setState({
  
      stepIndex: stepIndex + 1,
      finished: stepIndex >= 1,
    });
}
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return <Stepone/>;
      case 1:
        return <Steptwo/>;
      default:
        return 'Wrong Step bc';
    }
  }

	
  render() {
    const {finished, stepIndex} = this.state;
    

    return (
      <div >
        <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Fill in Your Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>Enter the subjects you want to give</StepLabel>
          </Step>
        </Stepper>
        <div >
            <div>
              { this.getStepContent(stepIndex)}
              <div style={{marginTop: 12}}>
                <FlatButton
                  label="Back"
                  disabled={stepIndex === 0}
                  onTouchTap={this.handlePrev}
                  style={{marginRight: 12}}
                />
                {stepIndex === 1 ? <Dialog onSubmit={this.handleSubmit.bind(this)} /> : 
                <RaisedButton
                  label='Next'
                  primary={true}
                  onTouchTap={this.handleNext}
                />
                }
              </div>
            </div>
        </div>
      </div>
    );
  }
}


export default Exam;
