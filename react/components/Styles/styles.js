import darkBaseTheme from 'material-ui/styles/baseThemes/darkBaseTheme';
import lightBaseTheme from 'material-ui/styles/baseThemes/lightBaseTheme';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const style = {
	titleBar: {
		backgroundColor: '#1976D2'
	},

	button: {
		backgroundColor: '#2196F3',
		color: '#ffffff'
	},
  	root: {
    	display: 'flex',
    	flexWrap: 'wrap',
    	justifyContent: 'space-around'
  	},
  	gridList: {
    	flex: '1',
    	overflowY: 'auto'
  	},
  	Selectyourform: {
  		font: 'Roboto',
  		align: 'center',
  		fontSize: '30px',
  		color: '#000'
  	}

	};

export default style; 