import React, { Component } from 'react';
import TextField from 'material-ui/TextField';
import Paper from 'material-ui/Paper';
import RaisedButton from 'material-ui/RaisedButton';
import { browserHistory } from 'react-router';

const style = {
  height: 150,
  width: 300,
  textAlign: 'center',
  
  flexWrap: 'wrap',
  justifyContent: 'space-around'
};

var paperStyle = {
	width: 'auto',
	alignContent: 'center',
	alignItems: 'center',
	marginTop: '15%',
	marginLeft: '35%',
	backgroundColor: 'transparent',
	WebkitBoxShadow: 'none',
	noxBxShadow: 'none',
	boxShadow: 'none'
	
};

const Username = "student";
const Password = "12345678";

class Login extends Component {
	state = {	
				Username: '',
				Password: ''
			}

	

	handleUsername = (event, Username) => {this.setState({Username})}
	handlePassword = (event, Password) => {this.setState({Password})}
	handleLogin = (event) => {
		if (this.state.Username== Username && this.state.Password == Password){
			browserHistory.push('/secpage')
		}
		else {
			console.log("Wrong Password")	
		}
	}
	render() {
		return (
			<div style={paperStyle}>
				<Paper style={style} zDepth = {3}>
					
					<TextField
          			hintText="Enter your Username"
          			value = {this.state.Username}
          			onChange={this.handleUsername}
        			/>

        			<TextField
          			hintText="Enter your Password"
          			value = {this.state.Password}
          			type="password"
          			onChange={this.handlePassword}
        			/>

        			<RaisedButton 
        				label="Login" 
        				primary={true} 
        				style={{backgroundColor: "#1976D2"}}
        				onClick={this.handleLogin.bind(this)}
        			/>


				</Paper>
			</div>
		);
	}
}

export default Login;
