import React from 'react';
import {Tabs, Tab} from 'material-ui/Tabs';
import SwipeableViews from 'react-swipeable-views';
import {List, ListItem} from 'material-ui/List';
import Subheader from 'material-ui/Subheader';
import Divider from 'material-ui/Divider';
import Checkbox from 'material-ui/Checkbox';
import Toggle from 'material-ui/Toggle';
import { observable } from 'mobx';
import { observer } from 'mobx-react'
import data from '../models/fetch';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import Dialog from './DialogforAdmin';

const styles = {
  headline: {
    fontSize: 24,
    paddingTop: 16,
    marginBottom: 12,
    fontWeight: 400

  },
  slide: {
    padding: 10
  },
};

const style = {
  marginRight: 20,
};

@observer
class Personform extends React.Component {  


  handleSubmit() {
  fetch("http://abhi:5000/emails",{
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({ emails: {emailid: this.candidate.adm_email}  })
}).then(r => console.log(r))
}


  @observable candidate = {}
   
    componentWillMount() {

      const x = this.props.location.pathname.split('/').pop()
      
      data.getData().then(()=> this.candidate = data.getSelectedData(x))
    
     
  
  }
  
  constructor(props) {
    super(props);
    this.state = {
      slideIndex: 0,
    };
  }

  handleChange = (value) => {
    this.setState({
      slideIndex: value,
    });
  };

  render() {

    
    const x = this.props.location.pathname.split('/').pop()
    const candidate = this.candidate || {}
    const fullname = `${candidate.adm_fname} ${candidate.adm_faname} ${candidate.adm_lname}`
    const address = `${candidate.adm_address1} ${candidate.adm_address2} ${candidate.adm_pincode}`
    const birthDetails = `${candidate.adm_bplace} ${candidate.adm_splace} ${candidate.adm_csplace}`
    const messageTemplate = `View the File that ${candidate.adm_fname} has uploaded `
    return (
      <div>
    
    
        <Tabs  
          onChange={this.handleChange}
          value={this.state.slideIndex}
        >
          <Tab label="Personal Details" value={0} />
          <Tab label="Academic Details" value={1} />
          <Tab label="Parent Details" value={2} />
        </Tabs>

        <SwipeableViews
          index={this.state.slideIndex}
          onChangeIndex={this.handleChange}
        >
          <div>
            
            
              <ListItem
                primaryText={fullname}
                secondaryText="Name"
              />

              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_dob}
                secondaryText="Date of Birth"
              />

              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_gender}
                secondaryText="Gender"
              />

              
              <Divider/>


              
              <ListItem
                primaryText={candidate.adm_phone}
                secondaryText="Phone"
              />
              
              <Divider/>

              <ListItem
                primaryText={candidate.adm_email}
                secondaryText="Email"
              />
              
              <Divider/>

              <ListItem
                primaryText={candidate.adm_branch}
                secondaryText="Branch"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_year}
                secondaryText="Year"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_blood}
                secondaryText="Blood Group"
              />
              
              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_religon}
                secondaryText="Religon"
              />

              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_caste}
                secondaryText="Caste"
              />
            
              <Divider/>
              
              <ListItem
                primaryText={address}
                secondaryText="Address"
              />

              <Divider/>
              
              <ListItem
                primaryText={birthDetails}
                secondaryText="Birth Details"
              />

              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_aadhar}
                secondaryText="Adhaar Card Number"
              />
              <ListItem>
              <a href = {`http://localhost:5000/admissions/custshow/${x}`}>
              {messageTemplate}
              </a>
              </ListItem>
              <Dialog
              onSubmit = {this.handleSubmit.bind(this)}
              fullWidth = {true}

              />

            <br />
          </div>
          <div style={styles.slide}>
           <ListItem
                primaryText={candidate.adm_ktfe}
                secondaryText="KTs in FE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_ktse}
                secondaryText="KTs in SE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_ktte}
                secondaryText="KTs in TE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_attemptfe}
                secondaryText="Attempts for FE"
              />

              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_attemptse}
                secondaryText="Attempts for SE"
              />

              <Divider/>
              
              <ListItem
                primaryText={candidate.adm_attemptte}
                secondaryText="Attempts for TE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_cgpafe}
                secondaryText="CGPA in FE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_cgpase}
                secondaryText="CGPA in SE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_cgpate}
                secondaryText="CGPA in TE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_exambodyfe}
                secondaryText="Exambody in FE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_exambodyse}
                secondaryText="Exambody in SE"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_exambodyte}
                secondaryText="Exambody in TE"
              />

              
          </div>
          <div style={styles.slide}>
            <ListItem
                primaryText={candidate.adm_parentname}
                secondaryText="Parents Name"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_relation}
                secondaryText="Relation with the Candidate"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_parentph}
                secondaryText="Parent Phone number"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_parentoffad}
                secondaryText="Parents Office Address"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_residencead}
                secondaryText="Parents Recidency Address"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_date}
                secondaryText="Date"
              />

              <Divider/>

              <ListItem
                primaryText={candidate.adm_place}
                secondaryText="Place"
              />

              
          </div>
        </SwipeableViews>
      </div>
    );
  }
}

export default Personform;
