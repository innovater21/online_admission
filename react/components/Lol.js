import React from 'react';
import {GridList, GridTile} from 'material-ui/GridList';
import IconButton from 'material-ui/IconButton';
import Subheader from 'material-ui/Subheader';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import style from './Styles/styles';
import Data from '../Data/gridFormData';
import { browserHistory } from 'react-router';

const FormGridlist = () => (
  <div style={style.root}>
    <GridList
      cellHeight={300}
      style={style.gridList}
    >
      {Data.map((tile) => (
        <GridTile
          key={tile.title}
          title={tile.title}
          subtitle={<span><b>{tile.Subheader}</b></span>}
          actionIcon={<IconButton></IconButton>}
          onClick={()=> { browserHistory.push(tile.navigate)
          }}
        >
          <img src={tile.img} />
        </GridTile>
      ))}
    </GridList>
  </div>
);

export default FormGridlist;