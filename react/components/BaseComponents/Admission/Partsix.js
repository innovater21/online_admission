import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Adm_Schema from '../../../models/Admission';
import { observer } from 'mobx-react';
import Years from '../../../Data/Selectdata';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';


@observer
class Partone extends Component {

	  handleName = (event,  adm_parentname) => {Adm_Schema.adm_data.adm_parentname = adm_parentname}
	  handleOccupation = (event,  adm_occ) => {Adm_Schema.adm_data.adm_occ = adm_occ;}
  	handleRelation = (event,  adm_relation) => {Adm_Schema.adm_data.adm_relation = adm_relation;}
    handleAddressOne = (event, adm_parentoffad) => {Adm_Schema.adm_data.adm_parentoffad = adm_parentoffad;}
    handleAddressTwo = (event, adm_residenceadd) => {Adm_Schema.adm_data.adm_residencead = adm_residenceadd;}
    handlePh = (event,  adm_parentph) => {Adm_Schema.adm_data.adm_parentph = adm_parentph;}
    handleDate = (event,  adm_date) => {Adm_Schema.adm_data.adm_date = adm_date;}
    handlePlace = (event,  adm_place) => {Adm_Schema.adm_data.adm_place = adm_place;}

	render() {
		return (
			<div>

        <TextField
          hintText="Enter the name of your Parent/Guardian"
          fullWidth = {true}
          errorText="This is a required file"
          onChange={this.handleName}
          value={Adm_Schema.adm_data.adm_parentname}
        />

        <br/>
         <TextField
          hintText="Relation with the Student"
          value={Adm_Schema.adm_data.adm_relation}
          onChange={this.handleRelation}
        />

        <br/>
         <TextField
          hintText="Occupation"
          value={Adm_Schema.adm_data.adm_occ}
          
          onChange={this.handleOccupation}
        />

        <br/>
        
        <TextField
          hintText="Office Address"
          fullWidth={true}
          
          value={Adm_Schema.adm_data.adm_parentoffad}
          onChange={this.handleAddressOne}
        />

        <TextField
          hintText="Residency Address"
          fullWidth={true}
          errorText="This is a required file"
          onChange={this.handleAddressTwo}
          value={Adm_Schema.adm_data.adm_residencead}
        />


        <br/>

        <TextField
          hintText="Parents/Guardians Phone Number"
          errorText="This is a required file"
          
          value={Adm_Schema.adm_data.adm_parentph}
         onChange={this.handlePh}
        />

        <br/>

        <TextField
          hintText="Date"
          value={Adm_Schema.adm_data.adm_date}
         onChange={this.handleDate}
        />

        <br/>

        <TextField
          hintText="Place"
          value={Adm_Schema.adm_data.adm_place}
         onChange={this.handlePlace}
        />

        	</div>
		);
	}

}

export default Partone;