import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Adm_Schema from '../../../models/Admission';
import { observer } from 'mobx-react';
import Kts from '../../../Data/numbersArray';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';


const kts = Kts.map(function(data){
	return (<MenuItem key={data} value={data} primaryText = {data} />)
})


@observer
class Partthree extends Component{
		
	handleFekt = (event,index, adm_ktfe ) => {Adm_Schema.adm_data.adm_ktfe = adm_ktfe}
	handleCGPA = (event, adm_cgpafe ) => {Adm_Schema.adm_data.adm_cgpafe = adm_cgpafe}
	handleAttempts = (event, adm_attemptfe ) => {Adm_Schema.adm_data.adm_attemptfe = adm_attemptfe}
	handleExambody = (event, adm_exambodyfe ) => {Adm_Schema.adm_data.adm_exambodyfe = adm_exambodyfe}



	render() {
		return (
			<div>

				<SelectField
		          floatingLabelText="Enter the Kts in First Year"
		          value={Adm_Schema.adm_data.adm_ktfe}
		          onChange={this.handleFekt}
		          errorText="This is a required file"
		        >
		          {kts}
		        </SelectField>


		        <TextField
          			hintText="Enter your FE CGPA"
          			value={Adm_Schema.adm_data.adm_cgpafe}
          			
          			onChange={this.handleCGPA}
        		/>
        		
        		<TextField
          			hintText="Enter your Attempts for FE"
          			value={Adm_Schema.adm_data.adm_attemptfe}
          			onChange={this.handleAttempts}
        		/>

        		<TextField
          			hintText="Enter your Exam body for Fe"
          			value={Adm_Schema.adm_data.adm_exambodyfe}
          			onChange={this.handleExambody}
        		/>
        		<br />

			</div>
		);
	}
}

export default Partthree