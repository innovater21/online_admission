import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Adm_Schema from '../../../models/Admission';
import { observer } from 'mobx-react';
import Kts from '../../../Data/numbersArray';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';


const kts = Kts.map(function(data){
	return (<MenuItem key={data} value={data} primaryText = {data} />)
})


@observer
class Partfour extends Component{
		
	handleKt = (event, index, adm_ktse ) => {Adm_Schema.adm_data.adm_ktse = adm_ktse}
	handleCGPA = (event, adm_cgpase ) => {Adm_Schema.adm_data.adm_cgpase = adm_cgpase}
	handleAttempts = (event, adm_attemptse ) => {Adm_Schema.adm_data.adm_attemptse = adm_attemptse}
	handleExambody = (event, adm_exambodyse ) => {Adm_Schema.adm_data.adm_exambodyse = adm_exambodyse}



	render() {
		return (
			<div>

				<SelectField
		          floatingLabelText="Enter the Kts in Second Year"
		          value={Adm_Schema.adm_data.adm_ktse}
		          onChange={this.handleKt}

		        >
		          {kts}
		        </SelectField>


		        <TextField
          			hintText="Enter your SE CGPA"
          			
          			value={Adm_Schema.adm_data.adm_cgpase}
          			onChange={this.handleCGPA}
        		/>
        		
        		<TextField
          			hintText="Enter your Attempts for SE"
          			
          			value={Adm_Schema.adm_data.adm_ktse}
          			onChange={this.handleAttempts}
        		/>

        		<TextField
          			hintText="Enter your Exam body for SE"
          			value={Adm_Schema.adm_data.adm_attemptse}
          			onChange={this.handleExambody}
        		/>
        		<br />

			</div>
		);
	}
}

export default Partfour;
