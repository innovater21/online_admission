
import React, { Component } from 'react';
import { observable } from 'mobx';
import { observer } from 'mobx-react';

import Dialog from 'material-ui/Dialog';
import CircularProgress from 'material-ui/CircularProgress';
import ErrorOutline from 'material-ui/svg-icons/alert/error-outline';
import FileIcon from 'material-ui/svg-icons/editor/insert-drive-file';
import FlatButton from 'material-ui/FlatButton';
import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';
import Subheader from 'material-ui/Subheader';
import UploadIcon from 'material-ui/svg-icons/file/cloud-upload';
import { List, ListItem } from 'material-ui/List';
import { browserHistory } from 'react-router';
import RaisedButton from 'material-ui/RaisedButton';

const style = {
	UploadIcon: {
		color: 'red'
	},
	input: {
		display: 'none'
	}
}

const iconButtonElement = (
	<IconButton
		touch={true}
		tooltip="more"
		tooltipPosition="bottom-left">
		<MoreVertIcon />
	</IconButton>
);

@observer
class Partseven extends Component {

		@observable dialogConfig = {
		isOpen: false
	};

	constructor(props) {
		super(props);
		this.upload = this.upload.bind(this);
		this.openFileInput = this.openFileInput.bind(this);
		this.state = { show: false };
		this.fileToUpload = null;
		this.fileUpload = this.fileUpload.bind(this);
		this.actions = [
			<FlatButton
				label="Cancel"
				primary={true}
				onTouchTap={this.closeDialog} />,
			<FlatButton
				label="Delete"
				primary={true}
				keyboardFocused={true}
				onTouchTap={this.remove} />
		];

	}

	rightIconMenu = (file) => {
		return (
			<IconMenu iconButtonElement={iconButtonElement}>
				<MenuItem onTouchTap={this.openDialog(file)}>Delete</MenuItem>
			</IconMenu>
		);
	}

	openFileInput = () => {
		this.refs.fileToUpload.click();
	}

	openDialog = (file) => {
		return () => {
			this.dialogConfig = {
				isOpen: true,
				file: file
			}
			console.log('openDialog > return', this.dialogConfig);
		}
	}

		closeDialog = () => {
		this.dialogConfig = {
			isOpen: false
		};
	}

	fileUpload = (url, params) => {
	const formData = new FormData();
	formData.append("image", params.file);

	const xhr = new XMLHttpRequest();
	xhr.withCredentials = true;

	xhr.upload.addEventListener("loadstart", params.onLoadStart, false);
	xhr.upload.addEventListener("error", params.onError, false);
	xhr.upload.addEventListener("abort", params.onAbort, false);
	xhr.upload.addEventListener("timeout", params.onTimeout, false);
	xhr.upload.addEventListener("progress", params.onProgress, false);
	xhr.upload.addEventListener("load", params.onLoad, false);
	xhr.upload.addEventListener("loadend", params.onLoadEnd, false);

	xhr.addEventListener("loadstart", params.onLoadStart, false);
	xhr.addEventListener("error", params.onError, false);
	xhr.addEventListener("abort", params.onAbort, false);
	xhr.addEventListener("timeout", params.onTimeout, false);
	xhr.addEventListener("progress", params.onProgress, false);
	xhr.addEventListener("load", params.onLoad, false);
	xhr.addEventListener("loadend", params.onLoadEnd, false);
	xhr.open("POST", url);

	xhr.send(formData);
}


	upload(e) {
		this.setState({ show: true, message: "Preparing to upload ...", icon: <CircularProgress /> });

		const params = {
			onProgress: () => {this.setState({ show: true, message: "Uploading file ...", icon: <CircularProgress /> })},
			onLoad: (res) => {
				if(res.target.status && res.target.status > 299) {
					this.setState({ show: true, message: "Failed to upload!", icon: <ErrorOutline /> });
				} else {
					this.setState({ show: false, message: null });
					if(this.props.onChange) this.props.onChange();
				}
			},
			onError: (res) => {
				this.setState({ show: true, message: "Failed to upload!", icon: <ErrorOutline /> });
			}
		};
		const file = this.refs.fileToUpload.files[0]
		this.fileUpload('http://localhost:5000/photos', {
			file,
			...params
		})
		// const xhr = new XMLHttpRequest();

		// xhr.upload.addEventListener("error", params.onError, false);
		// xhr.upload.addEventListener("progress", params.onProgress, false);
		// xhr.upload.addEventListener("load", params.onLoad, false);
		// xhr.open("POST", 'http://localhost:5000/photos');
		// xhr.send(formData);
	}


	render() {


		return (
			<div>
			<List>
				<Subheader>Files</Subheader>
				<ListItem
					onTouchTap={this.openFileInput}
					leftIcon={<UploadIcon style={style.UploadIcon} />}
					primaryText={this.fileName || "Click To Upload A File"}
					secondaryText="Image, Doc, PDF" />
				<input style={style.input} type="file" name="file" ref="fileToUpload" onChange={this.upload} />
			</List>

			<RaisedButton
                  label='Done'
                  primary={true}
                  onTouchTap ={ () => {browserHistory.push("/lol")}  }    
            />
            </div>
		);
	}
}

export default Partseven;