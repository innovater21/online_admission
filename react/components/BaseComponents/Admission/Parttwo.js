import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Adm_Schema from '../../../models/Admission';
import { observer } from 'mobx-react';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';


const items = Branch.map(function(data){
  return (<MenuItem key={data.title} value={data.title} primaryText = {data.title} />)
})

@observer
class Parttwo extends Component {
    

    handleAddressOne = (event, adm_address1) => {Adm_Schema.adm_data.adm_address1 = adm_address1;}
    handleAddressTwo = (event, adm_address2) => {Adm_Schema.adm_data.adm_address2 = adm_address2;}
    handlePincode = (event, adm_pincode ) => {Adm_Schema.adm_data.adm_pincode = adm_pincode}
    handlePhone = (event, adm_phone ) => {Adm_Schema.adm_data.adm_phone = adm_phone}
    handleEmail = (event, adm_email ) => {Adm_Schema.adm_data.adm_email = adm_email}
    handleBplace = (event, adm_bplace ) => {Adm_Schema.adm_data.adm_bplace = adm_bplace}
    handleBstate = (event, adm_splace ) => {Adm_Schema.adm_data.adm_splace = adm_splace}
    handleBcountry = (event, adm_cplace ) => {Adm_Schema.adm_data.adm_cplace = adm_cplace}
    handleAadhar = (event, adm_aadhar ) => {Adm_Schema.adm_data.adm_aadhar = adm_aadhar}
    handleBranch = (event,index, adm_branch ) => {Adm_Schema.adm_data.adm_branch = adm_branch}


    render() {
      return (
        <div>
    
        <TextField
          hintText="Address line 1"
          fullWidth={true}
          errorText="This is a required file"
          value={ Adm_Schema.adm_data.adm_address1 }
          onChange={this.handleAddressOne}
        />

        <TextField
          hintText="Address line 2"
          fullWidth={true}
          value={Adm_Schema.adm_data.adm_address2}
          onChange={this.handleAddressTwo}
        />
                
        <TextField
          hintText = "Pincode"
          onChange={this.handlePincode}
          errorText="This is a required file"
          value={Adm_Schema.adm_data.adm_pincode}
        />

        <TextField
          hintText = "Phone Number"
          onChange={this.handlePhone}
          errorText="This is a required file"
          value={Adm_Schema.adm_data.adm_phone}
        />
        
        <br/>

        <TextField
          hintText = "Email id"
          onChange={this.handleEmail}
          errorText="This is a required file"
          value={Adm_Schema.adm_data.adm_email}
        />

        <br/>

        <TextField
          hintText = "Birth Place"
          onChange={this.handleBplace}
          value={Adm_Schema.adm_data.adm_bplace}
        />

        <br/>

        <TextField
          hintText = "Birth State"
          onChange={this.handleBstate}
          value={Adm_Schema.adm_data.adm_splace}
        />
        <br/>
        
        <TextField
          hintText = "Birth Country"
          value={Adm_Schema.adm_data.adm_cplace}
          onChange={this.handleBcountry}
        />
        <br/>
        
        <TextField
          hintText = "Enter your aadhar card number"
          onChange={this.handleAadhar}
          errorText="This is a required file"
          value={Adm_Schema.adm_data.adm_aadhar}
        />

        <SelectField
              floatingLabelText="Enter your Branch"
              errorText="This is a required file"
              value={Adm_Schema.adm_data.adm_branch}
              onChange={this.handleBranch}

            >
              {items}
        </SelectField>

        </div>

    );
  }
}



export default Parttwo;