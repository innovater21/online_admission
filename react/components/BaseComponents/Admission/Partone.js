import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Adm_Schema from '../../../models/Admission';
import { observer } from 'mobx-react';
import Years from '../../../Data/Selectdata';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';
import Castes from '../../../Data/Caste'

const caste = Castes.map(function(data){
  return (<MenuItem key={data.value} value={data.title} primaryText = {data.title} />)
})

const List = Years.map(function(data){
	return (<MenuItem key={data.value} value={data.title} primaryText = {data.title} />)
})

const items = Branch.map(function(data){
  return (<MenuItem key={data.value} value={data.title} primaryText = {data.title} />)
})

@observer
class Partone extends Component {

	  handleYear = (event,index, adm_year) => {Adm_Schema.adm_data.adm_year = adm_year;}
	  handleFirst = (event, adm_fname) => {Adm_Schema.adm_data.adm_fname = adm_fname;}
  	handleMiddle = (event, adm_faname) => {Adm_Schema.adm_data.adm_faname = adm_faname;}
  	handleLast = (event, adm_lname) => {Adm_Schema.adm_data.adm_lname = adm_lname;}
  	handleMom = (event, yourmom) => {Adm_Schema.adm_data.adm_moname = yourmom;}
  	handleBirth = (event, adm_dob) => {Adm_Schema.adm_data.adm_dob = adm_dob;}
  	handleReligon = (event, adm_religon) => {Adm_Schema.adm_data.adm_religon = adm_religon;}
  	handlecaste = (event,index, adm_caste) => {Adm_Schema.adm_data.adm_caste = adm_caste;}
  	handleBlood = (event, adm_blood) => {Adm_Schema.adm_data.adm_blood = adm_blood;}
  	handleGender = (event, adm_gender) => {Adm_Schema.adm_data.adm_gender = adm_gender;}


	render() {
		return (
			<div>
			<SelectField
          floatingLabelText="Enter your Year"
          value={Adm_Schema.adm_data.adm_year}
          onChange={this.handleYear}

>
         		 {List}
        	</SelectField>

        <TextField
          hintText="First Name"
          value={Adm_Schema.adm_data.adm_fname}
          errorText="This is a required file"
          onChange={this.handleFirst}
        />

        <br/>
         <TextField
          hintText="Middle Name"
          value={Adm_Schema.adm_data.adm_fanamem}
          onChange={this.handleMiddle}
        />

        <br/>
         <TextField
          hintText="Last Name"
          value={Adm_Schema.adm_data.adm_lname}
          onChange={this.handleLast}
        />

        <br/>

        <TextField
          hintText="Mothers Name"
          value={Adm_Schema.adm_data.adm_moname}
          onChange={this.handleMom}
        />

        <br/>

        <TextField
          hintText="Date of Birth"
          value={Adm_Schema.adm_data.adm_dob}
          errorText="This is a required file"
          onChange={this.handleBirth}
        />

        <br/>

        <TextField
          hintText="Religon"
          value={Adm_Schema.adm_data.adm_religon}
          onChange={this.handleReligon}
        />

        <br/>

        
       <SelectField
          floatingLabelText="Enter your Caste"
          value={Adm_Schema.adm_data.adm_caste}
          errorText="This is a required file"
          onChange={this.handlecaste}

          >
             {caste}
          </SelectField>

        <br/>

        <TextField
          hintText="Blood Group"
          value={Adm_Schema.adm_data.adm_blood}
          onChange={this.handleBlood}
        />



        <br/>

        <TextField
          hintText="Enter your Gender (M/F)"
          value={Adm_Schema.adm_data.adm_gender}
          errorText="This is a required file"
         onChange={this.handleGender}
        />
        	</div>
		);
	}

}

export default Partone;