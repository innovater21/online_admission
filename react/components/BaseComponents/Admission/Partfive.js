import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Adm_Schema from '../../../models/Admission';
import { observer } from 'mobx-react';
import Kts from '../../../Data/numbersArray';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';


const kts = Kts.map(function(data){
	return (<MenuItem key={data} value={data} primaryText = {data} />)
})


@observer
class Partfive extends Component{
		
	handleKt = (event, index, adm_ktte ) => {Adm_Schema.adm_data.adm_ktte = adm_ktte}
	handleCGPA = (event, adm_cgpate ) => {Adm_Schema.adm_data.adm_cgpate = adm_cgpate}
	handleAttempts = (event, adm_attemptte ) => {Adm_Schema.adm_data.adm_attemptte = adm_attemptte}
	handleExambody = (event, adm_exambodyte ) => {Adm_Schema.adm_data.adm_exambodyte = adm_exambodyte}



	render() {
		return (
			<div>

				<SelectField
		          floatingLabelText="Enter the Kts in Third Year"
		          
		          value={Adm_Schema.adm_data.adm_ktte}
		          onChange={this.handleKt}

		        >
		          {kts}
		        </SelectField>


		        <TextField
          			hintText="Enter your TE CGPA"
          			
          			value={Adm_Schema.adm_data.adm_cgpate}
          			onChange={this.handleCGPA}
        		/>
        		
        		<TextField
          			hintText="Enter your Attempts for TE"
          			
          			value={Adm_Schema.adm_data.adm_attemptte}
          			onChange={this.handleAttempts}
        		/>

        		<TextField
          			hintText="Enter your Exam body for TE"
          			value={Adm_Schema.adm_data.adm_exambodyte}
          			onChange={this.handleExambody}
        		/>
        		<br />

			</div>
		);
	}
}

export default Partfive;
