import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PhotocopySchema from '../../../models/photocopy';
import { observer } from 'mobx-react';
import TextField from 'material-ui/TextField';



@observer
export default class Parttwo extends Component {

  handleSubjectName = (event, ph_subjectname) => {PhotocopySchema.photocopy_data.ph_subjectname = ph_subjectname;}
  handlePaperno = (event, ph_paperno) => {PhotocopySchema.photocopy_data.ph_paperno = ph_paperno;}
  handleCodeno = (event, ph_codeno) => {PhotocopySchema.photocopy_data.ph_codeno = ph_codeno;}
  handleDate = (event, ph_date) => {PhotocopySchema.photocopy_data.ph_date = ph_date}
  handleTime = (event, ph_time ) => {PhotocopySchema.photocopy_data.ph_time = ph_time}
  handleMarksObtained = (event, ph_marksobtained ) => {PhotocopySchema.photocopy_data.ph_marksobtained = ph_marksobtained}

  render() {
    return (
      <div >
        <TextField
          hintText="Enter Subject Text"
          value = {PhotocopySchema.photocopy_data.ph_subjectname}
          onChange={this.handleSubjectName}
        />
        <br/>

        <TextField
          hintText="Enter Paper number"
          errorText="This is a required file"
          value = {PhotocopySchema.photocopy_data.ph_paperno}
          onChange={this.handlePaperno}
        />
        <br/>
        <TextField
          hintText="Enter Examination Code"
          value = {PhotocopySchema.photocopy_data.ph_codeno}
          onChange={this.handleCodeno}
        />
        <br/>
        <TextField
          hintText = "Enter date of exam"
          value = {PhotocopySchema.photocopy_data.ph_date}
          onChange={this.handleDate}
        />
      
        
        <TextField
          hintText = "Enter time of the exam"
          value = {PhotocopySchema.photocopy_data.ph_time}
          onChange={this.handleTime}
        />
        
        
        <TextField
          hintText = "Marks Obtained"
          errorText="This is a required file"
          value = {PhotocopySchema.photocopy_data.ph_marksobtained}
          onChange={this.handleMarksObtained}
        />
        </div>
        
        );
	}
}

