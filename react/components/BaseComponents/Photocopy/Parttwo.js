import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PhotocopySchema from '../../../models/photocopy';
import { observer } from 'mobx-react';
import TextField from 'material-ui/TextField';



@observer
export default class Parttwo extends Component {

  handleCollegeName = (event, ph_nameco) => {PhotocopySchema.photocopy_data.ph_nameco = ph_nameco;}
  handleAddressOne = (event, ph_primaryaddco) => {PhotocopySchema.photocopy_data.ph_primaryaddco = ph_primaryaddco;}
  handleAddressTwo = (event, ph_secondaryaddco) => {PhotocopySchema.photocopy_data.ph_secondaryaddco = ph_secondaryaddco;}
  handleDistrict = (event, ph_districtco) => {PhotocopySchema.photocopy_data.ph_districtco = ph_districtco}
  handleState = (event, ph_stateco ) => {PhotocopySchema.photocopy_data.ph_stateco = ph_stateco}
  handlePincode = (event, ph_pincodeco ) => {PhotocopySchema.photocopy_data.ph_pincodeco = ph_pincodeco}

  render() {
    return (
      <div >
        <TextField
          hintText="Enter The Name of Your College"
          errorText="This is a required file"
          value = {PhotocopySchema.photocopy_data.ph_nameco}
          onChange={this.handleCollegeName}
        />
        

        <TextField
          hintText="Address line 1"
          fullWidth={true}
          value = {PhotocopySchema.photocopy_data.ph_primaryaddco}
          onChange={this.handleAddressOne}
        />
        <TextField
          hintText="Address line 2"
          fullWidth={true}
          value = {PhotocopySchema.photocopy_data.ph_secondaryaddco}
          onChange={this.handleAddressTwo}
        />

        <TextField
          hintText = "District"
          value = {PhotocopySchema.photocopy_data.ph_districtco}
          onChange={this.handleDistrict}
        />
       
        
        <TextField
          hintText = "State"
          value = {PhotocopySchema.photocopy_data.ph_stateco}
          onChange={this.handleState}
        />
        
        
        <TextField
          hintText = "Pincode"
          errorText="This is a required file"
          value = {PhotocopySchema.photocopy_data.ph_pincodeco}
          onChange={this.handlePincode}
        />
        </div>
        
        );
	}
}

