import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import PhotocopySchema from '../../../models/photocopy';
import { observer } from 'mobx-react';
import Mahina from '../../../Data/Mahina';
import Saal from '../../../Data/Saal';
import TextField from 'material-ui/TextField';

const Months = Mahina.map(function(data){
  return (<MenuItem key={data.value} value={data.value} primaryText = {data.title} />)})

const Years = Saal.map(function(data){
  return (<MenuItem key={data} value={data} primaryText = {data} />)})

@observer
export default class Partone extends Component {

  handleMonth = (event, index, ph_month) => {PhotocopySchema.photocopy_data.ph_month = ph_month;}
  handleYear = (event, index, ph_year) => {PhotocopySchema.photocopy_data.ph_year = ph_year;}
  handleCandidateName = (event, ph_nameca) => {PhotocopySchema.photocopy_data.ph_nameca = ph_nameca;}
  handleSeatNumber = (event, ph_seatnumber) => {PhotocopySchema.photocopy_data.ph_seatnumber = ph_seatnumber;}
  handleAddressOne = (event, ph_primaryaddca) => {PhotocopySchema.photocopy_data.ph_primaryaddca = ph_primaryaddca;}
  handleAddressTwo = (event, ph_secondaryaddca) => {PhotocopySchema.photocopy_data.ph_secondaryaddca = ph_secondaryaddca;}
  handleDistrict = (event, ph_districtca) => {PhotocopySchema.photocopy_data.ph_districtca = ph_districtca}
  handleState = (event, ph_stateca ) => {PhotocopySchema.photocopy_data.ph_stateca = ph_stateca}
  handlePincode = (event, ph_pincodeca ) => {PhotocopySchema.photocopy_data.ph_pincodeca = ph_pincodeca}

  render() {
    {console.log(PhotocopySchema.photocopy_data.ph_nameca)}
    return (
      <div >
        <SelectField
          floatingLabelText="Month"
          value={PhotocopySchema.photocopy_data.ph_month}
          onChange={this.handleMonth}

        >
          {Months}
        </SelectField>

        <SelectField
          floatingLabelText="Year"
          errorText="This is a required file"
          value={PhotocopySchema.photocopy_data.ph_year}
          onChange={this.handleYear}
        >
          {Years}
        </SelectField>
        <br/>
        <TextField
          hintText="Enter Your Name"
          errorText="This is a required file"
          value={PhotocopySchema.photocopy_data.ph_nameca}
          onChange={this.handleCandidateName}
        />
        <br/>
        <TextField
          hintText="Enter Your Seat Number"
          errorText="This is a required file"
          value={PhotocopySchema.photocopy_data.ph_seatnumber}
          onChange={this.handleSeatNumber}
        />
        <br />
        <TextField
          hintText="Address line 1"
          errorText="This is a required file"
          fullWidth={true}
          value={PhotocopySchema.photocopy_data.ph_primaryaddca}
          onChange={this.handleAddressOne}
        />
        <TextField
          hintText="Address line 2"
          fullWidth={true}
          value={PhotocopySchema.photocopy_data.ph_secondaryaddca}
          onChange={this.handleAddressTwo}
        />

        <TextField
          hintText = "District"
          value={PhotocopySchema.photocopy_data.ph_districtca}
          onChange={this.handleDistrict}
        />
       
        
        <TextField
          hintText = "State"
          value={PhotocopySchema.photocopy_data.ph_stateca}
          onChange={this.handleState}
        />
        
        
        <TextField
          hintText = "Pincode"
          errorText="This is a required file"
          value={PhotocopySchema.photocopy_data.ph_pincodeca}
          onChange={this.handlePincode}
        />
        </div>
        
        );
	}
}

