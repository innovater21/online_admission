import React, { Component } from 'react'; 
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import X from 'material-ui/AppBar';
import Stylebhai from '../Styles/styles';

const AppBar = () => (
  <X
  	style={Stylebhai.titleBar}
    title="Online Admission Portal"
  />
);

export default AppBar;