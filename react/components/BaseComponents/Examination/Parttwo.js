import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Exam_Schema from '../../../models/Examination';
import { observer } from 'mobx-react';
import TextField from 'material-ui/TextField';

@observer
export default class Parttwo extends Component {

handleSubject1 = (event, index, exam_subject1) => {Exam_Schema.exam_data.exam_subject1 = exam_subject1;}
handleSubject2 = (event, index, exam_subject2) => {Exam_Schema.exam_data.exam_subject2 = exam_subject2;}
handleSubject3 = (event, index, exam_subject3) => {Exam_Schema.exam_data.exam_subject3 = exam_subject3;}
handleSubject4 = (event, index, exam_subject4) => {Exam_Schema.exam_data.exam_subject4 = exam_subject4;}
handleSubject5 = (event, index, exam_subject5) => {Exam_Schema.exam_data.exam_subject5 = exam_subject5;}
handleSubject6 = (event, index, exam_subject6) => {Exam_Schema.exam_data.exam_subject6 = exam_subject6;}
handleSubject7 = (event, index, exam_subject7) => {Exam_Schema.exam_data.exam_subject7 = exam_subject7;}




  render() {
    return (
      <div>

        <TextField
          hintText="Name of Subject 1"
          value={Exam_Schema.exam_data.handleSubject1}
          errorText="This is a required file"
          onChange={this.handleSubject1}
        />
        <br/>

        <TextField
          hintText="Name of Subject 2"
          value={Exam_Schema.exam_data.exam_subject2}
          onChange={this.handleSubject2}
        />

        <br/>
        <TextField
          hintText="Name of Subject 3"
          value={Exam_Schema.exam_data.exam_subject3}
          onChange={this.handleSubject3}
        />

        <br/>
        <TextField
          hintText="Name of Subject 4"
          value={Exam_Schema.exam_data.exam_subject4}
          onChange={this.handleSubject4}
        />

        <br/>
        <TextField
          hintText="Name of Subject 5"
          value={Exam_Schema.exam_data.exam_subject5}
          onChange={this.handleSubject5}
        />

        <br/>
        <TextField
          hintText="Name of Subject 6"
          value={Exam_Schema.exam_data.exam_subject6}
          onChange={this.handleSubject6}
        />

        <br/>
        <TextField
          hintText="Name of Subject 7"
          value={Exam_Schema.exam_data.exam_subject7}
          onChange={this.handleSubject7}
        />

      </div>
    );
  }
  }
