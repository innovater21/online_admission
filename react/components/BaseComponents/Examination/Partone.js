import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Exam_Schema from '../../../models/Examination';
import { observer } from 'mobx-react';
import Sems from '../../../Data/Sems';
import Branch from '../../Selectdata1';
import TextField from 'material-ui/TextField';

const Sem = Sems.map(function(data){
  return (<MenuItem key={data} value={data} primaryText = {data} />)
})

const items = Branch.map(function(data){
  return (<MenuItem key={data.value} value={data.value} primaryText = {data.title} />)
})


@observer
export default class Partone extends Component {



  handleSem = (event, index, exam_sem) => {Exam_Schema.exam_data.exam_sem = exam_sem;}
  handleBranch = (event, index, exam_branch) => {Exam_Schema.exam_data.exam_branch = exam_branch;}
  handleScheme = (event, index, exam_scheme) => {Exam_Schema.exam_data.exam_scheme = exam_scheme;}
  handlePlace = (event, index, exam_place) => {Exam_Schema.exam_data.exam_place = exam_place;}
  handleDate = (event, index, exam_date) => {Exam_Schema.exam_data.exam_date = exam_date;}
  handleFirst = (event, index, exam_fname) => {Exam_Schema.exam_data.exam_fname = exam_fname;}
  handleMiddle = (event, index, exam_mname) => {Exam_Schema.exam_data.exam_mname = exam_mname;}
  handleLast = (event, index, exam_lname) => {Exam_Schema.exam_data.exam_lname = exam_lname;}
  handleAddress = (event, index, exam_address) => {Exam_Schema.exam_data.exam_address = exam_address;}
  handleTelnum = (event, index, exam_telnum) => {Exam_Schema.exam_data.exam_telnum = exam_telnum;}
  handleCaste = (event, index, exam_caste) => {Exam_Schema.exam_data.exam_caste = exam_caste;}

  render() {
    return (
      <div >
        <SelectField
          floatingLabelText="Enter your Year"
          errorText="This is a required file"
          value={Exam_Schema.exam_data.exam_sem}
          onChange={this.handleSem}

        >
          {Sem}
        </SelectField>
        <br/>
        <SelectField
          floatingLabelText="Branch"
          errorText="This is a required file"
          value={Exam_Schema.exam_data.exam_branch}
          onChange={this.handleBranch}
        >
          {items}
        </SelectField>
        <br/>

        <SelectField
          floatingLabelText="Exam Scheme"
          errorText="This is a required file"
          value={Exam_Schema.exam_data.exam_scheme}
          onChange={this.handleScheme}
        >
          <MenuItem value={1} primaryText="OLD" />
          <MenuItem value={2} primaryText="CBGS"/>
        
        </SelectField>
        <br/>

        <TextField
          hintText="Place"
          
          onChange={this.handlePlace}
        />

        <br/>
        <TextField
          hintText="Date"
          value={Exam_Schema.exam_data.exam_data}
          
          onChange={this.handleDate}
        />

        <br/>
         <TextField
          hintText="First Name"
          errorText="This is a required file"
          value={Exam_Schema.exam_data.exam_fname}
          onChange={this.handleFirst}
        />

        <br/>
         <TextField
          hintText="Middle Name"
          value={Exam_Schema.exam_data.exam_mname}
          onChange={this.handleMiddle}
        />

        <br/>
         <TextField
          hintText="Last Name"
          value={Exam_Schema.exam_data.exam_lname}
          onChange={this.handleLast}
        />

        <br/>
        <TextField
          hintText="Enter your Address"
          multiLine = {true}
          value={Exam_Schema.exam_data.exam_address}
          onChange={this.handleAddress}
        />

        <br/>
        <TextField
          hintText="Phone Number"
          errorText="This is a required file"
          value={Exam_Schema.exam_data.exam_telnum}
          onChange={this.handleTelnum}
        />

        <br/>
        <TextField
          hintText="Enter your Caste"
          errorText="This is a required file"
          value={Exam_Schema.exam_data.exam_caste}
          onChange={this.handleCaste}
        />
        
        </div>
        
        );
  }
}


