import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import KT_Schema from '../../../models/ktea';
import { observer } from 'mobx-react';
import TextField from 'material-ui/TextField';



@observer
export default class Parttwo extends Component {

  handleSubjectName = (event, ph_subjectname) => {KT_Schema.kt_data.ph_subjectname = ph_subjectname;}
  handlePaperno = (event, ph_paperno) => {KT_Schema.kt_data.ph_paperno = ph_paperno;}
  handleCodeno = (event, ph_codeno) => {KT_Schema.kt_data.ph_codeno = ph_codeno;}
  handleDate = (event, ph_date) => {KT_Schema.kt_data.ph_date = ph_date}
  handleTime = (event, ph_time ) => {KT_Schema.kt_data.ph_time = ph_time}
  handleMarksObtained = (event, ph_marksobtained ) => {KT_Schema.kt_data.ph_marksobtained = ph_marksobtained}

  render() {
    return (
      <div >
        <TextField
          hintText="Enter Subject Text"
          value = {KT_Schema.kt_data.ph_subjectname}
          onChange={this.handleSubjectName}
        />
        <br/>

        <TextField
          hintText="Enter Paper number"
          value = {KT_Schema.kt_data.ph_paperno}
          onChange={this.handlePaperno}
        />
        <br/>
        <TextField
          hintText="Enter Examination Code"
          value = {KT_Schema.kt_data.ph_codeno}
          onChange={this.handleCodeno}
        />
        <br/>
        <TextField
          hintText = "Enter date of exam"
          value = {KT_Schema.kt_data.ph_date}
          onChange={this.handleDate}
        />
      
        
        <TextField
          hintText = "Enter time of the exam"
          value = {KT_Schema.kt_data.ph_time}
          onChange={this.handleTime}
        />
        
        
        <TextField
          hintText = "Marks Obtained"
          value = {KT_Schema.kt_data.ph_marksobtained}
          onChange={this.handleMarksObtained}
        />
        </div>
        
        );
	}
}

