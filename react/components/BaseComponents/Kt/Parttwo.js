import React, { Component } from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import KT_Schema from '../../../models/ktea';
import { observer } from 'mobx-react';
import TextField from 'material-ui/TextField';



@observer
export default class Parttwo extends Component {

  handleCollegeName = (event, ph_nameco) => {KT_Schema.kt_data.ph_nameco = ph_nameco;}
  handleAddressOne = (event, ph_primaryaddco) => {KT_Schema.kt_data.ph_primaryaddco = ph_primaryaddco;}
  handleAddressTwo = (event, ph_secondaryaddco) => {KT_Schema.kt_data.ph_secondaryaddco = ph_secondaryaddco;}
  handleDistrict = (event, ph_districtco) => {KT_Schema.kt_data.ph_districtco = ph_districtco}
  handleState = (event, ph_stateco ) => {KT_Schema.kt_data.ph_stateco = ph_stateco}
  handlePincode = (event, ph_pincodeco ) => {KT_Schema.kt_data.ph_pincodeco = ph_pincodeco}

  render() {
    return (
      <div >
        <TextField
          hintText="Enter The Name of Your College"
          value = {KT_Schema.kt_data.ph_nameco}
          onChange={this.handleCollegeName}
        />
        

        <TextField
          hintText="Address line 1"
          errorText="This is a required file"
          fullWidth={true}
          value = {KT_Schema.kt_data.ph_primaryaddco}
          onChange={this.handleAddressOne}
        />
        <TextField
          hintText="Address line 2"
          fullWidth={true}
          value = {KT_Schema.kt_data.ph_secondaryaddco}
          onChange={this.handleAddressTwo}
        />

        <TextField
          hintText = "District"
          value = {KT_Schema.kt_data.ph_districtco}
          onChange={this.handleDistrict}
        />
       
        
        <TextField
          hintText = "State"
          value = {KT_Schema.kt_data.ph_stateco}
          onChange={this.handleState}
        />
        
        
        <TextField
          hintText = "Pincode"
          errorText="This is a required file"
          value = {KT_Schema.kt_data.ph_pincodeco}
          onChange={this.handlePincode}
        />
        </div>
        
        );
	}
}

