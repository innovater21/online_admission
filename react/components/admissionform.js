import React, { Component } from 'react';
import {
  Step,
  Stepper,
  StepLabel,
} from 'material-ui/Stepper';
import RaisedButton from 'material-ui/RaisedButton';
import FlatButton from 'material-ui/FlatButton';
import Stepone from './BaseComponents/Admission/Partone'
import Steptwo from './BaseComponents/Admission/Parttwo'
import Stepthree from './BaseComponents/Admission/Partthree'
import Stepfour from './BaseComponents/Admission/Partfour'
import Stepfive from './BaseComponents/Admission/Partfive'
import Stepsix from './BaseComponents/Admission/Partsix'
import Stepseven from './BaseComponents/Admission/Partseven'
import Admission_Schema from '../models/Admission';
import Dialog from './BaseComponents/Admission/DialogAdmission';
import { browserHistory } from 'react-router';


class Admission extends Component {

state = {
    finished: false,
    stepIndex: 0,
  };

  handleSubmit() {
  if(Admission_Schema.adm_data.adm_fname != "" && Admission_Schema.adm_data.adm_dob != "" &&
    Admission_Schema.adm_data.adm_caste != "" && Admission_Schema.adm_data.adm_gender != "" && 
    Admission_Schema.adm_data.adm_address1 != "" && Admission_Schema.adm_data.adm_pincode != "" &&
    Admission_Schema.adm_data.adm_phone != "" && Admission_Schema.adm_data.adm_email != "" && 
    Admission_Schema.adm_data.adm_aadhar != "" && Admission_Schema.adm_data.adm_branch != "" && 
    Admission_Schema.adm_data.adm_ktfe != "" && Admission_Schema.adm_data.adm_parentname != "" &&
    Admission_Schema.adm_data.adm_residenceadd != "" && Admission_Schema.adm_data.adm_parentph != "") 
     {
      if( Admission_Schema.adm_data.adm_ktfe > 0 && Admission_Schema.adm_data.adm_ktte >0) {
        window.alert("Wrong KT Details.")
        browserHistory.push("/admission")
      }
      if( Admission_Schema.adm_data.adm_ktfe + Admission_Schema.adm_data.adm_ktte + 
        Admission_Schema.adm_data.adm_ktse > 6) {
        window.alert("You have KTS more than 6. Please contact Examination Section and the Admission center")
        browserHistory.push("/admission")
      }
      else{
      fetch("http://localhost:5000/admissions",{
       method: 'POST',
      headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
       },
      body: JSON.stringify({ admissions: Admission_Schema.adm_data })
      }).then(r => console.log(r))

      }

  }   
  else {
    window.alert("The Required Fields have not been satisfied")
    browserHistory.push("/admission")
  }
}
  
  handleNext = () => {
    const {stepIndex} = this.state;
    if (stepIndex < 7) {
    this.setState({
  
      stepIndex: stepIndex + 1,
      finished: stepIndex >= 7,
    });
}
  };

  handlePrev = () => {
    const {stepIndex} = this.state;
    if (stepIndex > 0) {
      this.setState({stepIndex: stepIndex - 1});
    }
  };

  getStepContent(stepIndex) {
    switch (stepIndex) {
      case 0:
        return <Stepone/>;
      
      case 1:
        return <Steptwo/>;
      
      case 2:
        return <Stepthree/>;
      
      case 3:
        return <Stepfour/>;

     case 4:
        return <Stepfive/>;
      
      case 5:
        return <Stepsix/>;
    
       case 6:
        return <Stepseven/>;

      default:
        return 'Wrong Step ';
    }
  }


	
  render() {
    const {finished, stepIndex} = this.state;
    

    return (
      <div >
        <Stepper activeStep={stepIndex}>
          <Step>
            <StepLabel>Your Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>More Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>FE Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>SE Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>TE Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>Parent Details</StepLabel>
          </Step>
          <Step>
            <StepLabel>Upload Credentials</StepLabel>
          </Step>
        </Stepper>
        <div >
            <div>
                { this.getStepContent(stepIndex)}
              <div style={{marginTop: 12}}>
                <FlatButton
                  label="Back"
                  disabled={stepIndex === 0}
                  onTouchTap={this.handlePrev}
                  style={{marginRight: 12}}
                />
                {stepIndex === 5 ? <Dialog onSubmit={this.handleSubmit.bind(this)} /> : 
                <RaisedButton
                  label='Next'
                  primary={true}
                  onTouchTap={this.handleNext}
                />
                }
              </div>
            </div>
        </div>
      </div>
    );
  }
}


export default Admission;
