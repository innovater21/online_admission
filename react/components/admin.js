import React, { Component } from 'react';
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import { observable } from 'mobx';
import { observer } from 'mobx-react'
import CircularProgress from 'material-ui/CircularProgress';
import { browserHistory } from 'react-router';
import data from '../models/fetch';

@observer
class Admintable extends Component {


	
	componentWillMount() {
		data.getData()
	}



	render() {
		if (!data.candidatesData.length)
			return <CircularProgress/>

		const tbl = (data.candidatesData || []).filter( candidate => candidate.adm_caste == "Open").map(candidate => (
			<TableRow 
				data = {candidate}
				selectable={false}
			 	key={candidate.id} 
			 	onClick={()=> browserHistory.push(`personform/${candidate.id}`)} 
			 >
        		<TableRowColumn>{candidate.id}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_fname}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_branch}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_year}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_caste}</TableRowColumn>
      		</TableRow>
      		))

				const tbl1 = (data.candidatesData || []).filter( candidate => candidate.adm_caste == "SC/ST").map(candidate => (
			<TableRow 
				data = {candidate}
				selectable={false}
			 	key={candidate.id} 
			 	onClick={()=> browserHistory.push(`personform/${candidate.id}`)} 
			 >
        		<TableRowColumn>{candidate.id}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_fname}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_branch}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_year}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_caste}</TableRowColumn>
      		</TableRow>
      		))

			const tbl2 = (data.candidatesData || []).filter( candidate => candidate.adm_caste == "OBC").map(candidate => (
			<TableRow 
				data = {candidate}
				selectable={false}
			 	key={candidate.id} 
			 	onClick={()=> browserHistory.push(`personform/${candidate.id}`)} 
			 >
        		<TableRowColumn>{candidate.id}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_fname}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_branch}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_year}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_caste}</TableRowColumn>
      		</TableRow>
      		))

      		const tbl3 = (data.candidatesData || []).filter( candidate => candidate.adm_caste == "TFWS").map(candidate => (
			<TableRow 
				data = {candidate}
				selectable={false}
			 	key={candidate.id} 
			 	onClick={()=> browserHistory.push(`personform/${candidate.id}`)} 
			 >
        		<TableRowColumn>{candidate.id}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_fname}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_branch}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_year}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_caste}</TableRowColumn>
      		</TableRow>
      		))

      		const tbl4 = (data.candidatesData || []).filter( candidate => candidate.adm_caste == "NT/DT").map(candidate => (
			<TableRow 
				data = {candidate}
				selectable={false}
			 	key={candidate.id} 
			 	onClick={()=> browserHistory.push(`personform/${candidate.id}`)} 
			 >
        		<TableRowColumn>{candidate.id}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_fname}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_branch}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_year}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_caste}</TableRowColumn>
      		</TableRow>
      		))

      		const tbl5 = (data.candidatesData || []).filter( candidate => candidate.adm_caste == "Others").map(candidate => (
			<TableRow 
				data = {candidate}
				selectable={false}
			 	key={candidate.id} 
			 	onClick={()=> browserHistory.push(`personform/${candidate.id}`)} 
			 >
        		<TableRowColumn>{candidate.id}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_fname}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_branch}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_year}</TableRowColumn>
        		<TableRowColumn>{candidate.adm_caste}</TableRowColumn>
      		</TableRow>
      		))

      		
		return (
		  	<div>
		  	
			  <Table selectable={false}>
			    <TableHeader displaySelectAll={false} adjustForCheckbox = {false}>
			      <TableRow selectable= {false}>
			        <TableHeaderColumn>ID</TableHeaderColumn>
			        <TableHeaderColumn>Name</TableHeaderColumn>
			        <TableHeaderColumn>Branch</TableHeaderColumn>
			        <TableHeaderColumn>Year</TableHeaderColumn>
			        <TableHeaderColumn>Caste</TableHeaderColumn>
			      </TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} displayRowCheckbox={false}>
			    	{tbl}
			    </TableBody>
			  </Table>

			  <Table selectable={false}>
			    <TableHeader displaySelectAll={false} adjustForCheckbox = {false}>
			      <TableRow selectable= {false}>
			        <TableHeaderColumn>ID</TableHeaderColumn>
			        <TableHeaderColumn>Name</TableHeaderColumn>
			        <TableHeaderColumn>Branch</TableHeaderColumn>
			        <TableHeaderColumn>Year</TableHeaderColumn>
			        <TableHeaderColumn>Caste</TableHeaderColumn>
			      </TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} displayRowCheckbox={false}>
			    	{tbl1}
			    </TableBody>
			  </Table>

			  <Table selectable={false}>
			    <TableHeader displaySelectAll={false} adjustForCheckbox = {false}>
			      <TableRow selectable= {false}>
			        <TableHeaderColumn>ID</TableHeaderColumn>
			        <TableHeaderColumn>Name</TableHeaderColumn>
			        <TableHeaderColumn>Branch</TableHeaderColumn>
			        <TableHeaderColumn>Year</TableHeaderColumn>
			        <TableHeaderColumn>Caste</TableHeaderColumn>
			      </TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} displayRowCheckbox={false}>
			    	{tbl2}
			    </TableBody>
			  </Table>

			  <Table selectable={false}>
			    <TableHeader displaySelectAll={false} adjustForCheckbox = {false}>
			      <TableRow selectable= {false}>
			        <TableHeaderColumn>ID</TableHeaderColumn>
			        <TableHeaderColumn>Name</TableHeaderColumn>
			        <TableHeaderColumn>Branch</TableHeaderColumn>
			        <TableHeaderColumn>Year</TableHeaderColumn>
			        <TableHeaderColumn>Caste</TableHeaderColumn>
			      </TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} displayRowCheckbox={false}>
			    	{tbl3}
			    </TableBody>
			  </Table>

			  <Table selectable={false}>
			    <TableHeader displaySelectAll={false} adjustForCheckbox = {false}>
			      <TableRow selectable= {false}>
			        <TableHeaderColumn>ID</TableHeaderColumn>
			        <TableHeaderColumn>Name</TableHeaderColumn>
			        <TableHeaderColumn>Branch</TableHeaderColumn>
			        <TableHeaderColumn>Year</TableHeaderColumn>
			        <TableHeaderColumn>Caste</TableHeaderColumn>
			      </TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} displayRowCheckbox={false}>
			    	{tbl4}
			    </TableBody>
			  </Table>
			  
			  <Table selectable={false}>
			    <TableHeader displaySelectAll={false} adjustForCheckbox = {false}>
			      <TableRow selectable= {false}>
			        <TableHeaderColumn>ID</TableHeaderColumn>
			        <TableHeaderColumn>Name</TableHeaderColumn>
			        <TableHeaderColumn>Branch</TableHeaderColumn>
			        <TableHeaderColumn>Year</TableHeaderColumn>
			        <TableHeaderColumn>Caste</TableHeaderColumn>
			      </TableRow>
			    </TableHeader>
			    <TableBody showRowHover={true} displayRowCheckbox={false}>
			    	{tbl5}
			    </TableBody>
			  </Table>

			</div>
);
}
}

export default Admintable;