import React, { Component } from 'react'; 
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from '../BaseComponents/AppBar';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
import RaisedButton from 'material-ui/RaisedButton';
import styleBhai from  '../Styles/styles';
import { browserHistory } from 'react-router';

class App extends Component {
	
	render() { 

		return (
			<MuiThemeProvider>
				<div>
					    <CardMedia
      					overlay={<CardTitle title="Online Admission Portal"  />}
    					>
      					<img src="http://getmyuni.azureedge.net/college-all-images/atharva-college-of-engineering-ace-mumbai/images/3.JPG" />
    					</CardMedia>
    					<CardTitle subtitle="About our Project" />
    					<CardText>

      					Our project aims at developing an online college admission system that is of importance 
      					to the college as well as students. Admission process done manually is a time denoting process 
      					and also leads to long queues. In this advance era of technology, we are reducing the manual work 
      					and excess time taken in admission process by making it as an online process. 
      					Students can register into the system and can fill the admission forms. Students can also apply
      					for revaluation as well as photocopy forms. Notice regarding students who took the admission is 
      					also displayed on the window.
      				
    					</CardText>
					    <CardActions>
					      <RaisedButton 
					      onClick={() => browserHistory.push('/studentLogin')}
					      backgroundColor="#2196F3"
					      labelColor="#fff"
					      label="Start Your Application" 
					      fullWidth={true}
					       />
					    </CardActions>
					  

					  
					  
				</div>
			</MuiThemeProvider>


		);
	}
}

export default App ; 