import React , { Component } from 'react';
import ReactDOM from 'react-dom';
import { Router, Route, browserHistory, Link, IndexRoute} from 'react-router';
import App from './App';
import Lol from './Lol';
import Skeleton from './skeleton';
import Secpage from './secpage';
import injectTapEventPlugin from 'react-tap-event-plugin';
import Admission from './admissionform';
import Reval from './revalform';
import Photocopy from './photocopy';
import Ktform from './Ktform';
import XXX from '../testfetch';
import Admin from './admin';
import Personform from './Personform';
import Login from './adminLogin';
import studentLogin from './studentLogin';
import StepSeven from './BaseComponents/Admission/Partseven'

injectTapEventPlugin();


export default class Root extends Component {


	render() {
		return (
			<Router history={browserHistory} >
				<Route path = "/" component = {Skeleton}>
					<IndexRoute component = {App} />
					<Route path = "/home" component = {App} />
						<Route path = "/secpage" component = {Secpage} />
						<Route path = "/lol" component = {Lol} />
						<Route path = "/admission" component = {Admission} />
						<Route path = "/reval" component = {Reval} />
						<Route path = "/kt" component = {Ktform} />
						<Route path = "/photocopy" component = {Photocopy} />
						<Route path = "/test" component = {XXX}/>
						<Route path = "/admin" component = {Admin}/>
						<Route path = "/personform/:id" component = {Personform}/>
						<Route path = "/adminlogin" component = {Login}/>
						<Route path = "/studentlogin" component = {studentLogin} />
						<Route path = "/credentials" component = {StepSeven} />
      			</Route>
   			</Router>

		);
	}
}

