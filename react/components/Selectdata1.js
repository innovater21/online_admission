const dataSelect = 
[
	{
		title: 'Computers',
		value: 1
	},
	{
		title: 'Information Technology',
		value: 2
	},
	{
		title: 'Electrical',
		value: 3
	},
	{
		title: 'Electronics',
		value: 4
	},
	{
		title: 'Electronics and Telecommunication',
		value: 5
	}

]

export default dataSelect;