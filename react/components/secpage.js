import React, { Component } from 'react'; 
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import Year from '../Data/Selectdata';
import Branch from './Selectdata1';
import Kts from '../Data/numbersArray';
import Dialog from './BaseComponents/dialogsecondpage';
import 'whatwg-fetch';
import { browserHistory } from 'react-router';

const List = Year.map(function(data){
	return (<MenuItem key={data.value} value={data.value} primaryText = {data.title} />)
})

const items = Branch.map(function(data){
	return (<MenuItem key={data.value} value={data.value} primaryText = {data.title} />)
})

const kts = Kts.map(function(data){
	return (<MenuItem key={data} value={data} primaryText = {data} />)
})



class Secpage extends Component {

	state = {
				year_ent : null,
				branch_ent : null,
				fe_ent : null,
				se_ent : null,
				te_ent : null
			};

handleSubmit() {
  if(this.state.fe_ent + this.state.se_ent + this.state.te_ent < 6) {
    if(this.state.year_ent != null && this.state.branch_ent != null){
    fetch("http://192.168.1.6:5000/validations",{
  method: 'POST',
  headers: {
    'Accept': 'application/json',
    'Content-Type': 'application/json'
  },
  body: JSON.stringify({ validations: this.state })
}).then(r => console.dir(r, "fed"))
  } 
  else{
    window.alert("required fields not satisfied")
    browserHistory.push("/secpage")
  }
}else {
    window.alert("Please contact examination section for admission. Even if you try to fill the form. We Will Drop It :)");
    
  }

}
	
handleChange = (event, index, year_ent) => {
		this.setState({year_ent});
	}

handleChangee = (event, index, branch_ent) => {
		this.setState({branch_ent});
	}

handleChange1 = (event, index, fe_ent) => {
		console.log({fe_ent})
		this.setState({fe_ent});
	}

handleChange2 = (event, index, se_ent) => {
		console.log({se_ent})
		this.setState({se_ent});
	}

handleChange3 = (event, index, te_ent) => {
		this.setState({te_ent});
  }


	render() {


		return(
			<div style={{
    					height:'100%'
						}}>
				<SelectField
				 	
          			floatingLabelText="Enter your Year"
                errorText="This is a required file"
         			value={this.state.year_ent}
          			onChange={this.handleChange}
  					>
  						{List}
        		</SelectField>

        		<br />

        		<SelectField
          		floatingLabelText="Enter your Department"
              errorText="This is a required file"
         		value={this.state.branch_ent}
          		onChange={this.handleChangee}
  				>
  					{items}
        		</SelectField>
        		<br />

        		<SelectField
          		floatingLabelText="Kts in First Year"
              
         		value={this.state.fe_ent}
          		onChange={this.handleChange1}
  				>
  					{kts}
        		</SelectField>
        		<br />

        		<SelectField
          		floatingLabelText="Kts in Second Year"
              
         		value={this.state.se_ent}
          		onChange={this.handleChange2}
  				>
  					{kts}
        		</SelectField>
        		<br />

        		<SelectField
          		floatingLabelText="Kts in Third Year"
              
         		 value={this.state.te_ent}
          		onChange={this.handleChange3}
  				>
  					{kts}
        		</SelectField>

        		<Dialog onSubmit={this.handleSubmit.bind(this)} />
		</div>
		);
	}
}

export default Secpage;
