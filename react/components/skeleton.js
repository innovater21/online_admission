import React, { Component } from 'react';
import Appbar from './BaseComponents/AppBar';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import {Card, CardActions, CardHeader, CardMedia, CardTitle, CardText} from 'material-ui/Card';
class Skeleton extends Component {
	render() {
		return (
			<MuiThemeProvider>
				<div className="display">
				
					<Appbar/>
					<Card 
					
					> 

					{this.props.children}
					</Card>
				</div>
			</MuiThemeProvider>	
		);
	}
}

export default Skeleton;
