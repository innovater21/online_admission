from ruby:2.3.3
RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN mkdir /myapp
WORKDIR /myapp
ADD Gemfile /myapp/Gemfile
ADD Gemfile.lock /myapp/Gemfile.lock

RUN (groupadd -g 1000 app && adduser --system --uid=1000 --gid=1000 --home /home/app --shell /bin/bash app)

RUN echo Asia/Kolkata | tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

RUN bundle install
Add . /myapp