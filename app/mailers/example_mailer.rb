class ExampleMailer < ApplicationMailer
	default from: "samplemailer111@gmail.com"

  def sample_email(user)
    @user = user
    mail(to: @user.emailid, subject: 'Sample Email')
  end
end


