class ApplicationMailer < ActionMailer::Base
  default from: 'samplemailer111@gmail.com'
  layout 'mailer'
end
