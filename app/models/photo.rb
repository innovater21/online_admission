class Photo < ApplicationRecord
	
    do_not_validate_attachment_file_type :image 
    has_attached_file :image
            
end
