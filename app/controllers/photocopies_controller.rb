class PhotocopiesController < ApplicationController
	def index
		@testdata=Photocopie.all
		render json: @testdata
	end

	def show
		@Photocopie = Photocopie.find(params[:id])
		render json: @Photocopie
	end

	def create
		params.permit!
    	@photocopie = Photocopie.new(params[:photocopies])	
    	@photocopie.save
	end

end
