class PhotosController < ApplicationController
	def index
    	@photos = Photo.order('created_at')
  	end

  	def new
    	@photo = Photo.new
  	end

  	def create
      params.permit!
      @photo1 = Admission.last
    	@photo = Photo.new(image: params[:image],admission_id: @photo1.id)
    	if @photo.save
      		flash[:success] = "The photo was added!"
      		redirect_to root_path
    	else
      		render 'new'
    	end
  	end

    private

  	# def photo_params
   #  	params.require(:photo).permit(:image, :title)
  	# end
end
