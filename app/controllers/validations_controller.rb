class ValidationsController < ApplicationController
	def index
		@testdata=Validation.all
		render json: @testdata
	end

	def show
		@validation = Validation.find(params[:id])
		render json: @validation
	end

	def new
	end

	def edit
		@validation = Validation.find(params[:id])
	end
	
	def create
		params.permit!
    	@validation = Validation.new(params[:validations])	
    	@validation.save
	end

	def destroy
	    @validation = Validation.find(params[:id])
	    @validation.destroy
	 
	    redirect_to validation_path
	end
end
