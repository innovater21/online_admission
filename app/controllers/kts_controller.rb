class KtsController < ApplicationController
	def index
		@testdata=Kt.all
		render json: @testdata
	end

	def show
		@kt = Kt.find(params[:id])
		render json: @kt
	end

	def create
		params.permit!
    	@kt = Kt.new(params[:kts])	
    	@kt.save
	end

end
