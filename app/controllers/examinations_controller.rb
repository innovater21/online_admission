class ExaminationsController < ApplicationController
	def index
		@testdata=Examination.all
		render json: @testdata
	end

	def show
		@examination = Examination.find(params[:id])
		render json: @examination
	end

	def create
		params.permit!
    	@examination = Examination.new(params[:examinations])	
    	@examination.save
	end

end
