Rails.application.routes.draw do
  resources :uploads
  resources :users
  get 'validations/index'

  root 'validations#index'

  resources :validations

  resources :photocopies

  resources :kts

  resources :examinations

  resources :admissions

  get ':admissions/:custshow/:id' => 'admissions#custshow'

  resources :users

  resources :uploads

  resources :emails

  resources :photos, only: [:new, :create, :index, :destroy]

  


  

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end

