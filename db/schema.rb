# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170228070047) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "admissions", force: :cascade do |t|
    t.string   "adm_year"
    t.string   "adm_fname"
    t.string   "adm_faname"
    t.string   "adm_lname"
    t.string   "adm_moname"
    t.string   "adm_gender"
    t.string   "adm_dob"
    t.string   "adm_religon"
    t.string   "adm_blood"
    t.string   "adm_caste"
    t.string   "adm_address1"
    t.string   "adm_address2"
    t.string   "adm_pincode"
    t.string   "adm_phone"
    t.string   "adm_email"
    t.string   "adm_bplace"
    t.string   "adm_splace"
    t.string   "adm_csplace"
    t.string   "adm_aadhar"
    t.string   "adm_branch"
    t.string   "adm_ktfe"
    t.string   "adm_ktse"
    t.string   "adm_ktte"
    t.string   "adm_cgpafe"
    t.string   "adm_cgpase"
    t.string   "adm_cgpate"
    t.string   "adm_attemptfe"
    t.string   "adm_attemptse"
    t.string   "adm_attemptte"
    t.string   "adm_exambodyfe"
    t.string   "adm_exambodyse"
    t.string   "adm_exambodyte"
    t.string   "adm_parentname"
    t.string   "adm_relation"
    t.string   "adm_occ"
    t.string   "adm_parentph"
    t.string   "adm_parentoffad"
    t.string   "adm_residencead"
    t.string   "adm_date"
    t.string   "adm_place"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  create_table "attachments", force: :cascade do |t|
    t.integer  "admission_id"
    t.string   "file_file_name"
    t.string   "file_content_type"
    t.integer  "file_file_size"
    t.datetime "file_updated_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "emails", force: :cascade do |t|
    t.string   "emailid"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "examinations", force: :cascade do |t|
    t.string   "exam_sem"
    t.string   "exam_branch"
    t.string   "exam_scheme"
    t.string   "exam_place"
    t.string   "exam_date"
    t.string   "exam_fname"
    t.string   "exam_mname"
    t.string   "exam_lname"
    t.string   "exam_address"
    t.string   "exam_telnum"
    t.string   "exam_caste"
    t.string   "exam_subject1"
    t.string   "exam_subject2"
    t.string   "exam_subject3"
    t.string   "exam_subject4"
    t.string   "exam_subject5"
    t.string   "exam_subject6"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "kts", force: :cascade do |t|
    t.integer  "ph_month"
    t.integer  "ph_year"
    t.integer  "ph_seatnumber"
    t.integer  "ph_nameca"
    t.integer  "ph_primaryaddca"
    t.integer  "ph_secondaryaddca"
    t.integer  "ph_districtca"
    t.integer  "ph_stateca"
    t.integer  "ph_pincodeca"
    t.integer  "ph_nameco"
    t.integer  "ph_primaryaddco"
    t.integer  "ph_secondaryaddco"
    t.integer  "ph_districtco"
    t.integer  "ph_stateco"
    t.integer  "ph_pincodeco"
    t.integer  "ph_subjectname"
    t.integer  "ph_paperno"
    t.integer  "ph_codeno"
    t.integer  "ph_date"
    t.integer  "ph_time"
    t.integer  "ph_marksobtained"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "photocopies", force: :cascade do |t|
    t.integer  "ph_month"
    t.integer  "ph_year"
    t.string   "ph_seatnumber"
    t.string   "ph_nameca"
    t.string   "ph_primaryaddca"
    t.string   "ph_secondaryaddca"
    t.string   "ph_districtca"
    t.string   "ph_stateca"
    t.string   "ph_pincodeca"
    t.string   "ph_nameco"
    t.string   "ph_primaryaddco"
    t.string   "ph_secondaryaddco"
    t.string   "ph_districtco"
    t.string   "ph_stateco"
    t.string   "ph_pincodeco"
    t.string   "ph_subjectname"
    t.string   "ph_paperno"
    t.string   "ph_codeno"
    t.string   "ph_date"
    t.string   "ph_time"
    t.integer  "ph_marksobtained"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "photos", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.datetime "image_updated_at"
    t.integer  "admission_id"
  end

  create_table "uploads", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.string   "attach_file_name"
    t.string   "attach_content_type"
    t.integer  "attach_file_size"
    t.datetime "attach_updated_at"
    t.string   "pic_file_name"
    t.string   "pic_content_type"
    t.integer  "pic_file_size"
    t.datetime "pic_updated_at"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "validations", force: :cascade do |t|
    t.integer  "year_ent"
    t.integer  "branch_ent"
    t.integer  "fe_ent"
    t.integer  "se_ent"
    t.integer  "te_ent"
    t.integer  "be_ent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
