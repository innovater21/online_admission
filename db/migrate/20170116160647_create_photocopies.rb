class CreatePhotocopies < ActiveRecord::Migration[5.0]
  def change
    create_table :photocopies do |t|
    	t.integer :ph_month 
		t.integer :ph_year 
		t.string :ph_seatnumber
		t.string :ph_nameca 
		t.string :ph_primaryaddca 
		t.string :ph_secondaryaddca 
		t.string :ph_districtca 
		t.string :ph_stateca 
		t.string :ph_pincodeca 
		t.string :ph_nameco 
		t.string :ph_primaryaddco 
		t.string :ph_secondaryaddco 
		t.string :ph_districtco 
		t.string :ph_stateco 
		t.string :ph_pincodeco 
		t.string :ph_subjectname 
		t.string :ph_paperno 
		t.string :ph_codeno 
		t.string :ph_date 
		t.string :ph_time 
		t.integer :ph_marksobtained 

      t.timestamps
    end
  end
end
