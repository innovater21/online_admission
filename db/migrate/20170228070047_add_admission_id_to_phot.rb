class AddAdmissionIdToPhot < ActiveRecord::Migration[5.0]
  def change
  	add_column :photos, :admission_id, :integer
  end
end
