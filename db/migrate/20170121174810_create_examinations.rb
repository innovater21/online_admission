class CreateExaminations < ActiveRecord::Migration[5.0]
  def change
    create_table :examinations do |t|
        t.string :exam_sem
		t.string :exam_branch
		t.string :exam_scheme
		t.string :exam_place
		t.string :exam_date
		t.string :exam_fname
		t.string :exam_mname
		t.string :exam_lname
		t.string :exam_address
		t.string :exam_telnum
		t.string :exam_caste
		t.string :exam_subject1
		t.string :exam_subject2
		t.string :exam_subject3
		t.string :exam_subject4
		t.string :exam_subject5
		t.string :exam_subject6

      t.timestamps
    end
  end
end
