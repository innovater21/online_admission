class CreateKts < ActiveRecord::Migration[5.0]
  def change
    create_table :kts do |t|
    	t.integer :ph_month
		t.integer :ph_year 
		t.integer :ph_seatnumber 
		t.integer :ph_nameca 
		t.integer :ph_primaryaddca 
		t.integer :ph_secondaryaddca 
		t.integer :ph_districtca 
		t.integer :ph_stateca 
		t.integer :ph_pincodeca 
		t.integer :ph_nameco 
		t.integer :ph_primaryaddco 
		t.integer :ph_secondaryaddco 
		t.integer :ph_districtco 
		t.integer :ph_stateco 
		t.integer :ph_pincodeco 
		t.integer :ph_subjectname 
		t.integer :ph_paperno 
		t.integer :ph_codeno 
		t.integer :ph_date 
		t.integer :ph_time 
		t.integer :ph_marksobtained 
      t.timestamps
    end
  end
end
