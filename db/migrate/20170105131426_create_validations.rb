class CreateValidations < ActiveRecord::Migration[5.0]
  def change
    create_table :validations do |t|
      t.integer :year_ent
      t.integer :branch_ent
      t.integer :fe_ent
      t.integer :se_ent
      t.integer :te_ent
      t.integer :be_ent

      t.timestamps
    end
  end
end
