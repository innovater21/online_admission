class CreateAdmissions < ActiveRecord::Migration[5.0]
  def change
    create_table :admissions do |t|
    	t.string :adm_year
		t.string :adm_fname
		t.string :adm_faname
		t.string :adm_lname
		t.string :adm_moname
		t.string :adm_gender
		t.string :adm_dob
		t.string :adm_religon
		t.string :adm_blood
		t.string :adm_caste
		t.string :adm_address1
		t.string :adm_address2
		t.string :adm_pincode
		t.string :adm_phone
		t.string :adm_email
		t.string :adm_bplace
		t.string :adm_splace
		t.string :adm_csplace
		t.string :adm_aadhar
		t.string :adm_branch
		t.string :adm_ktfe
		t.string :adm_ktse
		t.string :adm_ktte
		t.string :adm_cgpafe
		t.string :adm_cgpase
		t.string :adm_cgpate
		t.string :adm_attemptfe
		t.string :adm_attemptse
		t.string :adm_attemptte
		t.string :adm_exambodyfe
		t.string :adm_exambodyse
		t.string :adm_exambodyte
		t.string :adm_parentname
		t.string :adm_relation
		t.string :adm_occ
		t.string :adm_parentph
		t.string :adm_parentoffad
		t.string :adm_residencead
		t.string :adm_date
		t.string :adm_place

      t.timestamps
    end
  end
end
