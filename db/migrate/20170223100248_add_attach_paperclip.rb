class AddAttachPaperclip < ActiveRecord::Migration[5.0]
  	def self.up
  		add_column :uploads, :attach_file_name,    :string
  		add_column :uploads, :attach_content_type, :string
  		add_column :uploads, :attach_file_size,    :integer
  		add_column :uploads, :attach_updated_at,   :datetime
  	end
   	def self.down
   		remove_column :uploads, :attach_file_name
   		remove_column :uploads, :attach_content_type
   		remove_column :uploads, :attach_file_size
   		remove_column :uploads, :attach_updated_at
   	end
end
